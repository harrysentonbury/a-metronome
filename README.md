# *a-metronome*

A command line metronome that you can output on any device. (USB audio interface
for example) or just your computer or whatever. Set any BPM and volume.
Run with no args for default 90 beats per minute
*or bars per minute with -p (pattern) flag*.
With a long BPM it can be used to generate a tone for tuning
etc, accepts cycles per second or a midi note number.

```
pip3 install numpy
pip3 install sounddevice
```

**To run:-**

```
python3 a_metronome.py [options...]
```

usage: a_metronome.py [-h] [-b [BPM]] [-t] [-r [RATIO]] [-v [VOLUME]] [-l]
                      [-d [DEVICE]]

+ -h, --help            show this help message and exit
+ -b [BPM], --bpm [BPM] int, beats per minute (default: 90)
+ -t, --two             add a second beat
+ -r [RATIO], --ratio [RATIO] float, ratio of second beat 0 to 1.0 (default: 0.5)
+ -v [VOLUME], --volume [VOLUME] volume of sound 0 to 1.0 (default: 0.7)
+ -f [FREQUENCY], --frequency [FREQUENCY] float, frequency of sound (default: 329.6276)
+ -s [samplerate], --samplerate [SAMPLERATE] int, samplerate 44100 or 48000 (default: 48000)
+ -m [MIDINUM], --midinum [MIDINUM] int, midi note number (default: 69)
+ -p [PATTERNLIST [PATTERNLIST ...]], --patternlist [PATTERNLIST [PATTERNLIST ...]]
...1=beat 0=miss a beat, default=[1, 1, 1, 1] (default: None)

+ -l, --listdevices     list output devices and exit
+ -d [DEVICE], --device [DEVICE] output device ID number or string
+ -s [SAMPLERATE], --samplerate [SAMPLERATE] int, samplerate 44100 or 48000 (default: 48000)
+ -x [XSTRING], --xstring [XSTRING] a word for history command to grep (default: None)

(default: default device)

![single](images/single.jpg)

**A pair of beats per minute.**

A second beat at a lower volume can be added for a pair of beats per minute.
The second beat can have any time ratio.

![double](images/double.jpg)

**Examples**

70 bars per minute with a second at 5/8 of a beat.

```
python3 a_metronome.py -b 70 -t -r 0.625
```

30 bars per minute, note is A6 (midi), beat pattern 1 0 0 0 1 0 1 0 1 0 1 1 0 0
and a samplerate of 44100

```
python3 a_metronome.py -b 30 -m 93 -p 1 0 0 0 1 0 1 0 1 0 1 1 0 0 -s 44100

```

Stick any string on the end so you can easily grep it in command history after an
**-x** flag.

```
python3 a_metronome.py -b 30 -m 93 -p 1 0 0 0 1 0 1 0 1 0 1 1 0 0 -s 44100 -x guitar0

```

**USB interface etc**

List available devices.

```
python3 a_metronome.py -l
```

Pick a device from the list and add the device name or number to a command
after a **-d** flag.

```
python3 a_metronome.py -b 32 -f 880 -p 1 0 0 0 1 0 1 0 1 0 1 1 0 0 -d 4
```

32 bars per minute, the frequency of A5, the beat pattern and device number 4.
*Note* Always check the device number of the audio USB interface or whatever
because it may have a different number every time it is plugged in.
