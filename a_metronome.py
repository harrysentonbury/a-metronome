#!/usr/bin/env python3

# Copyright (C) 2021 harrysentonbury
# GNU General Public License v3.0

import argparse
import numpy as np
import sounddevice as sd


def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text


parser = argparse.ArgumentParser(prog="a_metronome.py",
                                 description="Produces audible clicks at a regular \
                                         interval or regular patterns",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", "--bpm", nargs="?", help="int, beats per minute",
                    type=int, default=90)
parser.add_argument("-t", "--two", action="store_true", help="add a second beat")
parser.add_argument("-r", "--ratio", nargs="?",
                    help="float, ratio of second beat 0 to 1.0", type=float, default=0.5)
parser.add_argument("-v", "--volume", nargs="?",
                    help="volume of sound 0 to 1.0", type=float, default=0.7)
parser.add_argument("-f", "--frequency", nargs="?", type=float, default=659.2551,
                    help="float, frequency of sound")
parser.add_argument("-m", "--midinum", nargs="?", type=int, default=76,
                    help="int, midi note number")
parser.add_argument("-p", "--patternlist", nargs="*", dest='patternlist',
                    action='store', type=int,
                    help="1=beat 0=miss a beat")
parser.add_argument("-l", "--listdevices", action="store_true",
                    help="list output devices and exit")
parser.add_argument("-d", "--device", nargs="?",
                    help="output device ID number or string", type=int_or_str)
parser.add_argument("-s", "--samplerate", nargs="?", help="int, samplerate 44100 or 48000",
                    type=int, default=48000)
parser.add_argument("-x", "--xstring", nargs="?", type=str,
                    help="a word for history command to search")
args = parser.parse_args()

if  args.midinum is None or args.frequency is None or args.bpm is None or \
    args.volume is None or args.ratio is None:
    parser.error("missing arguement")
if args.ratio > 1 or args.ratio < 0:
    parser.error("ratio must be between 0 and 1.0")
if args.volume > 1 or args.volume < 0:
    parser.error("volume must be between 0 and 1.0")
if args.bpm > 5000 or args.bpm < 1:
    parser.error("bpm between 1 and 5000")
if args.frequency > 18000 or args.frequency < 10:
    parser.error("frequency out of range must be 10 to 18000")
if args.midinum > 128 or args.midinum < 0:
    parser.error("midinote must be 0 to 128")
if args.samplerate != 44100 and args.samplerate != 48000:
    parser.error("samplerate must be 44100 or 48000")



class ThyRythmer():
    """
    apply a percussion envelope to audio numpy array
    data - 1d numpy array
    bar_size - int, number of samples per bar Eg:
        for 1 second of 48kH audio bar_size=48000
    attack - int, attack size in samples default=100
    flip - boolean, reverse beat shape, default=False
    decay_log_base - float, log base of decay
    decay_min - float, <= 1, sets decay_log_base of minimum decay value, default=-1
    pattern - python list, 1=beat 0=miss a beat, default=[1, 1, 1, 1]
    """
    def __init__(self, data, bar_size, attack=100, flip=False,
                 decay_min=-1, decay_log_base=10, pattern=[1, 1, 1, 1]):
        self.data = data
        self.bar_size = bar_size
        self.flip = flip
        self.attack = attack
        if decay_min > 1:
            raise ValueError("Must be <= 1")
        self.decay_min = decay_min
        self.decay_log_base = decay_log_base
        self.pattern = pattern
        self.beats_per_bar = np.size(self.pattern)
        if type(self.pattern) is not list:
            raise TypeError(f"pattern must be a list of integers {type(self.pattern)}")


    def beater(self):
            self.data = self.data / np.max(np.absolute(self.data))
            self.duration = np.size(self.data) / self.bar_size
            self.beats_duration = int(self.duration * self.beats_per_bar)
            self.extra = np.zeros(np.size(self.data) % self.beats_duration)
            self.ramp_decay = np.logspace(1, self.decay_min, int(np.size(
                self.data) // self.beats_duration),
                base=self.decay_log_base) / self.decay_log_base
            self.ramp_attack = np.linspace(self.ramp_decay[-1], 1, self.attack)
            self.ramp_decay[:self.attack] *= self.ramp_attack
            self.beats = np.array([])
            self.decay_end_val = np.zeros(np.size(self.ramp_decay))
            self.decay_end_val[:] = self.ramp_decay[-1]
            for i in range(int(self.beats_duration / self.beats_per_bar)):
                self.pattern.extend(self.pattern)
            for i in range(self.beats_duration):
                if self.pattern[i] >= 1:
                    self.beats = np.concatenate((self.beats, self.ramp_decay))
                else:
                    self.beats = np.concatenate((self.beats, self.decay_end_val))
            if self.flip:
                self.beats = np.flip(self.beats)
            self.beats = np.concatenate((self.beats, self.extra))
            return self.data * self.beats


def callback(outdata, frames, time, status):
    try:
        data = next(sound_slice)
        outdata[:] = data.reshape(-1, 1)
    except ValueError:
        print("cb error")
        outdata[:] = np.zeros((blocksize, 1))

def gen():
    """generate chunks sound and refill sound array"""
    sound = np.zeros(blocksize)
    while True:
        slice = sound[:blocksize]
        yield slice
        sound = sound[blocksize:]
        if np.size(sound) < (1024):
            sound = np.concatenate((sound, sound_1))

def tone(freq):
    x = np.linspace(0, 2 * np.pi * duration, arr_size)
    return np.sin(x * freq)


try:
    if args.listdevices:
        # device list
        print(sd.query_devices())
    else:
        duration = 60 / args.bpm
        if args.midinum and args.frequency == 659.2551:
            frequency = 440 * (2**((args.midinum - 69) / 12))
        elif args.frequency:
            frequency = args.frequency

        blocksize = 512
        sample_rate = args.samplerate
        attack_size = 700
        arr_size = int(duration * sample_rate)
        roll_amount = int(arr_size * args.ratio)

        sound_1 = tone(frequency)

        # envelope
        if args.patternlist:
            # print(f"pattern: {args.patternlist}")
            sound_beat = ThyRythmer(data=sound_1, bar_size=arr_size, decay_min=-6,
                        attack=attack_size, pattern=args.patternlist)
            sound_1 = sound_beat.beater()
        else:
            sound_beat = ThyRythmer(data=sound_1, bar_size=arr_size, decay_min=-6,
                        attack=attack_size, pattern=[1])
            sound_1 = sound_beat.beater()

        if args.two:
            sound_2 = np.roll(sound_1, roll_amount) * 0.5
            sound_1 = sound_1 + sound_2

        sound_1 = sound_1 / np.max(np.abs(sound_1))
        sound_1 = sound_1 * args.volume * 0.3

        # stream
        sound_slice = gen()
        if args.device:
            device = args.device
        else:
            device = None
        with sd.OutputStream(device=device, channels=1, callback=callback,
                             blocksize=blocksize, samplerate=sample_rate):
            print(f"Samplerate: {sample_rate}")
            print("\npress the Return to exit :)")
            input()
except KeyboardInterrupt:
    parser.exit("\ninterupted by user :(")
except Exception as e:
    parser.exit(f"{type(e).__name__}: {str(e)}")
